
public class Swap {

	public static void main(String[] args) {
		swap2Variables(8, 3);
	}

	public static void swap2Variables(int num1, int num2) {
		System.out.println("Before swapping: ");
		System.out.println("num1= " + num1);
		System.out.println("num2= " + num2);
		// int temp=num1;
		num1 = num1 ^ num2;
		num2 = num1 ^ num2;
		num1 = num1 ^ num2;
		System.out.println("After swapping: ");
		System.out.println("num1= " + num1);
		System.out.println("num2= " + num2);
	}

}
