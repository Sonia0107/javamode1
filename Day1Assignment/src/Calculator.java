
public class Calculator {

	public static void main(String[] args) {
		add2Numbers(20, 4);
		subtract2Numbers(20, 4);
		multiply2Numbers(20, 4);
		divide2Numbers(20, 4);
		remainderOf2Numbers(20, 4);
	}

	public static void add2Numbers(int num1, int num2) {
		System.out.println("sum of " + num1 + " and " + num2 + " is " + (num1 + num2));
	}

	public static void subtract2Numbers(int num1, int num2) {
		System.out.println("sum of " + num1 + " and " + num2 + " is " + (num1 - num2));
	}

	public static void multiply2Numbers(int num1, int num2) {
		if (num2 != 0) {
			System.out.println("sum of " + num1 + " and " + num2 + " is " + (num1 * num2));
		} else {
			System.out.println("Cannot divide by zero");
		}
	}

	public static void divide2Numbers(int num1, int num2) {
		System.out.println("sum of " + num1 + " and " + num2 + " is " + (num1 / num2));
	}

	public static void remainderOf2Numbers(int num1, int num2) {
		System.out.println("sum of " + num1 + " and " + num2 + " is " + (num1 % num2));
	}

}
