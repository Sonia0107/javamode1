
public class Prime {

	public static void main(String[] args) {
		Prime exercise = new Prime();
		System.out.println(exercise.checkPrime(2));

	}

	public String checkPrime(int num) {
		if (num > 0) {
			if (num == 1) {
				return "1 is neither prime nor composite";

			}
			for (int i = 2; i < num; i++) {
				if (num % i == 0) {
					return num + " is not a prime number";
				}

			}
			return num + " is a prime number";
		} else {
			return "Enter a positive number";
		}

	}

}
