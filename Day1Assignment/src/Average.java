
public class Average {

	public static void main(String[] args) {

		averageOf3Numbers(3, 5, 8);
	}

	public static void averageOf3Numbers(double num1, double num2, double num3) {
		System.out.println("average of " + num1 + ", " + num2 + " and " + num3 + " is " + ((num1 + num2 + num3) / 3));
	}
}
