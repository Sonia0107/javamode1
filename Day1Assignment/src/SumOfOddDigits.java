
public class SumOfOddDigits {

	public static void main(String[] args) {
		sumOfOddDigits(123);

	}

	public static int sumOfOddDigits(int num) {

		int sum = 0;
		while (num > 0) {
			int digit = num % 10;
			num = num / 10;
			if (digit % 2 != 0) {
				sum += digit;
			}

		}

		if (sum % 2 == 0) {
			System.out.println("Sum of odd digits is even");
			return -1;
		} else {
			System.out.println("Sum of odd digits is odd");
			return 1;
		}
	}

}
