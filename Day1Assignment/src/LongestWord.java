
public class LongestWord {

	public static void main(String[] args) {
System.out.println("Longest word in the sentence : "+getLargestWord("I am a good girl"));
	}
	public static String getLargestWord(String sentence) {
		String[] words=sentence.split("\\s");
		int maxLength=0;
		String longestWord="";
		for (String word:words) {
			if(word.length()>maxLength) {
				maxLength=word.length();
				longestWord=word;
				
			}
			
		}
		return longestWord;
	}

}
