
public class SumOfSquareOfEvenDigits {

	public static void main(String[] args) {
		sumOfSquaresOfEvenDigits(44);

	}

    public static int sumOfSquaresOfEvenDigits(int num) {
        int sum = 0;
        if(num>0) {
            int temp=num;

            while (temp > 0) {

                int digit = temp % 10;
                temp /=  10;
                if (digit % 2 == 0) {
                    sum += Math.pow(digit, 2);
                }

            }

        }
        System.out.println("Sum of squares of even digits of "+num+" is "+sum);
        return sum;
    }
}
		





