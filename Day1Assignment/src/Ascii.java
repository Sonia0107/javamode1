
public class Ascii {

	public static void main(String[] args) {

		findAsciiValue('A');
	}

	public static void findAsciiValue(char ch) {
		int asciiValue = ch;
		System.out.println("ASCII value of '" + ch + "' : " + asciiValue);

	}
}
